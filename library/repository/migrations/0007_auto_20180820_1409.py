# Generated by Django 2.1 on 2018-08-20 08:39

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repository', '0006_auto_20180820_1406'),
    ]

    operations = [
        migrations.AlterField(
            model_name='record',
            name='available',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]
