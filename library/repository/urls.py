from django.contrib import admin
from django.urls import path, re_path, include
from . import views
from django.contrib.auth.views import(
    LoginView,
    LogoutView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView
    )
from rest_framework import routers

router = routers.DefaultRouter()
router.register('display', views.RecordView)

urlpatterns = [
    path('', views.index),
    path('login/', LoginView.as_view(template_name="repository/login.html"), name="login"),
    path('logout/', LogoutView.as_view(template_name="repository/logout.html"), name="logout"),
    path('register/', views.register, name='register'),
    path('profile/', views.view_profile, name='profile'),
    path('edit_profile/', views.edit_profile, name='profile'),
    path('change_password/', views.change_password, name='change_password'),
    path('reset_password/', PasswordResetView.as_view(template_name="repository/password_reset.html"), name="password_reset"),
    path('reset_password/done/', PasswordResetDoneView.as_view(template_name="repository/password_reset_done.html"), name="password_reset_done"),
    re_path(r'^reset_password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', PasswordResetConfirmView.as_view(template_name="repository/password_reset_confirm.html"), name="password_reset_confirm"),
    path('reset_password/complete/', PasswordResetCompleteView.as_view(template_name="repository/password_reset_complete.html"), name="password_reset_complete"),
    path('api_root/', include(router.urls)),
]
