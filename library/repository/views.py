from django.shortcuts import render, redirect
from repository.forms import RegistrationForm, EditProfileForm
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required

from rest_framework import viewsets
from .models import Record
from .serializers import RecordSerializer

class RecordView(viewsets.ModelViewSet):
    queryset = Record.objects.all()
    serializer_class = RecordSerializer
# Create your views hereself
def index(request):
    return render(request, 'repository/homepage.html')

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/repository/login/')
    else:
        form = RegistrationForm()
    args={'form':form}
    return render(request, 'repository/reg_form.html', args)

@login_required
def view_profile(request):
    args = {'user': request.user}
    return render(request, 'repository/profile.html', args)

@login_required
def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            # user = form.save()
            # user.refresh_from_db()
            # user.User_profile.image = form.cleaned_data.get('image')
            # user.save()
            return redirect('/repository/profile/')
    else:
        form = EditProfileForm(instance=request.user)
    args={'form':form}
    return render(request, 'repository/edit_profile.html', args)

@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('/repository/profile/')
        else:
            return redirect('/repository/change_password/')
    else:
        form = PasswordChangeForm(user=request.user)
    args={'form':form}
    return render(request, 'repository/change_password.html', args)
