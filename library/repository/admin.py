from django.contrib import admin
from . import models

class User_profileAdmin(admin.ModelAdmin):
    list_display = ('user', 'city', 'phone')

class RecordAdmin(admin.ModelAdmin):
    list_display = ('book', 'available')

admin.site.register(models.Record, RecordAdmin)
admin.site.register(models.User_profile, User_profileAdmin)
admin.site.site_header = 'Administration'
# Register your models here.
